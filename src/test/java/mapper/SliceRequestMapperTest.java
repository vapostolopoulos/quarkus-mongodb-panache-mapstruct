package mapper;

import dto.CloudConstraints;
import dto.CpuConstraint;
import dto.EmbbSliceProfileInstance;
import dto.NetworkingConstraint;
import dto.SliceIntent;
import dto.SliceRequest;
import dto.SliceRequester;
import dto.StorageConstraint;
import dto.UrllcSliceProfileInstance;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class SliceRequestMapperTest {

    @Inject
    SliceRequestMapper mapper;

    @Test
    void shouldMapSliceRequestWithToSliceRequestEntity() throws MalformedURLException {
        final var cloudConstraints = new CloudConstraints(null, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of());
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithURLLCToSliceRequestEntity() throws MalformedURLException {
        final var urllcSliceProfileInstance = new UrllcSliceProfileInstance(9999.9f);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), urllcSliceProfileInstance);
        final var cloudConstraints = new CloudConstraints(null, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithURLLCAndCpuConstraintToSliceRequestEntity() throws MalformedURLException {
        final var urllcSliceProfileInstance = new UrllcSliceProfileInstance(9999.9f);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), urllcSliceProfileInstance);
        final var cpuConstraint = new CpuConstraint(UUID.randomUUID().toString(), 8, "amount");
        final var cloudConstraints = new CloudConstraints(cpuConstraint, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithURLLCAndStorageConstraintToSliceRequestEntity() throws MalformedURLException {
        final var urllcSliceProfileInstance = new UrllcSliceProfileInstance(9999.9f);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), urllcSliceProfileInstance);
        final var storageConstraint = new StorageConstraint(UUID.randomUUID().toString(), 30, "gb");
        final var cloudConstraints = new CloudConstraints(null, storageConstraint);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithURLLCAndCloudConstraintsToSliceRequestEntity() throws MalformedURLException {
        final var urllcSliceProfileInstance = new UrllcSliceProfileInstance(9999.9f);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), urllcSliceProfileInstance);
        final var cpuConstraint = new CpuConstraint(UUID.randomUUID().toString(), 8, "amount");
        final var storageConstraint = new StorageConstraint(UUID.randomUUID().toString(), 30, "gb");
        final var cloudConstraints = new CloudConstraints(cpuConstraint, storageConstraint);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithEMBBToSliceRequestEntity() throws MalformedURLException {
        final var embbSliceProfileInstance = new EmbbSliceProfileInstance(100, 50);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), embbSliceProfileInstance);
        final var cloudConstraints = new CloudConstraints(null, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }


    @Test
    void shouldMapSliceRequestWithEMBBAndCpuConstraintToSliceRequestEntity() throws MalformedURLException {
        final var embbSliceProfileInstance = new EmbbSliceProfileInstance(41, 56);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), embbSliceProfileInstance);
        final var cpuConstraint = new CpuConstraint(UUID.randomUUID().toString(), 8, "amount");
        final var cloudConstraints = new CloudConstraints(cpuConstraint, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints, List.of(networkingConstraint));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }

    @Test
    void shouldMapSliceRequestWithEMBBAndURLLCToSliceRequestEntity() throws MalformedURLException {
        final var urllcSliceProfileInstance = new UrllcSliceProfileInstance(88.5654f);
        final var embbSliceProfileInstance = new EmbbSliceProfileInstance(100, 50);
        final var networkingConstraint = new NetworkingConstraint(UUID.randomUUID().toString(), embbSliceProfileInstance);
        final var networkingConstraint1 = new NetworkingConstraint(UUID.randomUUID().toString(), urllcSliceProfileInstance);
        final var cloudConstraints = new CloudConstraints(null, null);
        final var sliceIntent = new SliceIntent(UUID.randomUUID(), new URL("http://host:1024"), cloudConstraints,
                List.of(networkingConstraint, networkingConstraint1));
        final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        final var mappedSliceRequest = mapper.toModel(sliceRequestEntity);

        assertThat(mappedSliceRequest).usingRecursiveComparison().isEqualTo(sliceRequest);
    }
}
