package resource;


import dto.SliceIntent;
import dto.SliceRequest;
import dto.SliceRequester;
import org.jboss.logging.Logger;
import service.SliceService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

@Path("/api/v1/slice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SliceResource {

    private static final Logger LOGGER = Logger.getLogger(SliceResource.class);

    @Inject
    SliceService service;

    @GET
    @Path("{id}")
    public Response getSliceIntent(@PathParam("id") UUID sliceId) {
        SliceIntent sliceIntent = null;
        try {
            final var sliceRequest = service.get(sliceId);
            sliceIntent = sliceRequest.getSliceIntent();
        } catch (Exception e) {
            LOGGER.error("error: ", e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Status.OK).entity(sliceIntent).build();
    }

    @POST
    public Response addSliceIntent(SliceIntent sliceIntent) {
        sliceIntent.setId(UUID.randomUUID());

        SliceIntent addedSliceIntent = null;
        try {
            LOGGER.infof("Received %s", sliceIntent);
            final var sliceRequest = new SliceRequest(sliceIntent, SliceRequester.INTERNAL);

            final var addedSliceRequest = service.add(sliceRequest);

            addedSliceIntent = addedSliceRequest.getSliceIntent();
        } catch (Exception e) {
            LOGGER.error("error: ", e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Status.OK).entity(addedSliceIntent).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteSliceIntent(@PathParam("id") UUID sliceId) {
        try {
            service.delete(sliceId);
        } catch (Exception e) {
            LOGGER.error("error: ", e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.status(Status.OK).build();
    }
}
