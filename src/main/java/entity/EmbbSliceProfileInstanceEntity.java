package entity;

import dto.SliceServiceType;

public class EmbbSliceProfileInstanceEntity extends SliceProfileInstanceEntity {

    private double downlinkThroughput;
    private double uplinkThroughput;

    protected EmbbSliceProfileInstanceEntity() {
    }

    public EmbbSliceProfileInstanceEntity(double downlinkThroughput, double uplinkThroughput) {
        super(SliceServiceType.EMBB);
        this.downlinkThroughput = downlinkThroughput;
        this.uplinkThroughput = uplinkThroughput;
    }

    public double getDownlinkThroughput() {
        return downlinkThroughput;
    }

    public void setDownlinkThroughput(double downlinkThroughput) {
        this.downlinkThroughput = downlinkThroughput;
    }

    public double getUplinkThroughput() {
        return uplinkThroughput;
    }

    public void setUplinkThroughput(double uplinkThroughput) {
        this.uplinkThroughput = uplinkThroughput;
    }
}
