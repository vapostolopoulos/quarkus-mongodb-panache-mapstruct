package entity;

import java.util.List;

public class SliceIntentEntity {

    private String id;
    private String callbackUrl;
    private CloudConstraintsEntity cloudConstraints;
    private List<NetworkingConstraintEntity> networkingConstraints;

    protected SliceIntentEntity() {
    }

    public SliceIntentEntity(String id, String callbackUrl, CloudConstraintsEntity cloudConstraints, List<NetworkingConstraintEntity> networkingConstraints) {
        this.id = id;
        this.callbackUrl = callbackUrl;
        this.cloudConstraints = cloudConstraints;
        this.networkingConstraints = networkingConstraints;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public CloudConstraintsEntity getCloudConstraints() {
        return cloudConstraints;
    }

    public void setCloudConstraints(CloudConstraintsEntity cloudConstraints) {
        this.cloudConstraints = cloudConstraints;
    }

    public List<NetworkingConstraintEntity> getNetworkingConstraints() {
        return networkingConstraints;
    }

    public void setNetworkingConstraints(List<NetworkingConstraintEntity> networkingConstraints) {
        this.networkingConstraints = networkingConstraints;
    }
}
