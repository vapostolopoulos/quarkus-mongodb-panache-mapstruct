package entity;

public class CloudConstraintsEntity {

    private CpuConstraintEntity cpuConstraint;
    private StorageConstraintEntity storageConstraint;

    protected CloudConstraintsEntity() {
    }

    public CloudConstraintsEntity(CpuConstraintEntity cpuConstraint, StorageConstraintEntity storageConstraint) {
        this.cpuConstraint = cpuConstraint;
        this.storageConstraint = storageConstraint;
    }

    public CpuConstraintEntity getCpuConstraint() {
        return cpuConstraint;
    }

    public void setCpuConstraint(CpuConstraintEntity cpuConstraint) {
        this.cpuConstraint = cpuConstraint;
    }

    public StorageConstraintEntity getStorageConstraint() {
        return storageConstraint;
    }

    public void setStorageConstraint(StorageConstraintEntity storageConstraint) {
        this.storageConstraint = storageConstraint;
    }
}
