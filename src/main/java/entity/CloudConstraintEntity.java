package entity;

public abstract class CloudConstraintEntity {

    protected String id;
    protected int value;
    protected String unit;

    protected CloudConstraintEntity() {
    }

    protected CloudConstraintEntity(String id, int value, String unit) {
        this.id = id;
        this.value = value;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
