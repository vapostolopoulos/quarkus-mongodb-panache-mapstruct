package entity;

public class StorageConstraintEntity extends CloudConstraintEntity {

    protected StorageConstraintEntity() {
    }

    public StorageConstraintEntity(String id, int value, String unit) {
        super(id, value, unit);
    }
}
