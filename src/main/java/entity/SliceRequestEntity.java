package entity;

import dto.SliceRequester;
import io.quarkus.mongodb.panache.common.MongoEntity;

@MongoEntity
public class SliceRequestEntity {

    private SliceIntentEntity sliceIntent;
    private SliceRequester sliceRequester;

    protected SliceRequestEntity() {
    }

    public SliceRequestEntity(SliceIntentEntity sliceIntent, SliceRequester sliceRequester) {
        this.sliceIntent = sliceIntent;
        this.sliceRequester = sliceRequester;
    }

    public SliceIntentEntity getSliceIntent() {
        return sliceIntent;
    }

    public void setSliceIntent(SliceIntentEntity sliceIntent) {
        this.sliceIntent = sliceIntent;
    }

    public SliceRequester getSliceRequester() {
        return sliceRequester;
    }

    public void setSliceRequester(SliceRequester sliceRequester) {
        this.sliceRequester = sliceRequester;
    }
}
