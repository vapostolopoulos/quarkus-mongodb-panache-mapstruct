package entity;

import dto.SliceServiceType;

public class UrllcSliceProfileInstanceEntity extends SliceProfileInstanceEntity {

    private float upperDelayBoundInMs;

    protected UrllcSliceProfileInstanceEntity() {
    }

    public UrllcSliceProfileInstanceEntity(float upperDelayBoundInMs) {
        super(SliceServiceType.URLLC);
        this.upperDelayBoundInMs = upperDelayBoundInMs;
    }

    public float getUpperDelayBoundInMs() {
        return upperDelayBoundInMs;
    }

    public void setUpperDelayBoundInMs(float upperDelayBoundInMs) {
        this.upperDelayBoundInMs = upperDelayBoundInMs;
    }
}
