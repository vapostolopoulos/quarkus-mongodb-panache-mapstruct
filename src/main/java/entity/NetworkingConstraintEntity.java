package entity;

public class NetworkingConstraintEntity {

    private String endpointId;

    private SliceProfileInstanceEntity sliceProfileInstance;

    protected NetworkingConstraintEntity() {
    }

    public NetworkingConstraintEntity(String endpointId, SliceProfileInstanceEntity sliceProfileInstance) {
        this.endpointId = endpointId;
        this.sliceProfileInstance = sliceProfileInstance;
    }

    public String getEndpointId() {
        return endpointId;
    }

    public void setEndpointId(String endpointId) {
        this.endpointId = endpointId;
    }

    public SliceProfileInstanceEntity getSliceProfileInstance() {
        return sliceProfileInstance;
    }

    public void setSliceProfileInstance(SliceProfileInstanceEntity sliceProfileInstance) {
        this.sliceProfileInstance = sliceProfileInstance;
    }
}
