package entity;

public class CpuConstraintEntity extends CloudConstraintEntity {

    protected CpuConstraintEntity() {
    }

    public CpuConstraintEntity(String id, int value, String unit) {
        super(id, value, unit);
    }

}
