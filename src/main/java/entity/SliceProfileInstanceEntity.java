package entity;

import dto.SliceServiceType;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;

@BsonDiscriminator
public abstract class SliceProfileInstanceEntity {

    protected SliceServiceType sliceServiceType;

    protected SliceProfileInstanceEntity() {
    }

    protected SliceProfileInstanceEntity(SliceServiceType sliceServiceType) {
        this.sliceServiceType = sliceServiceType;
    }

    public SliceServiceType getSliceServiceType() {
        return sliceServiceType;
    }

    public void setSliceServiceType(SliceServiceType sliceServiceType) {
        this.sliceServiceType = sliceServiceType;
    }
}
