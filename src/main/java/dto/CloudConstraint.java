package dto;

public abstract class CloudConstraint {

    protected final String id;

    protected final int value;

    protected final String unit;

    protected CloudConstraint(String id, int value, String unit) {
        this.id = id;
        this.value = value;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return String.format(
                "{id:\"%s\", value:%d, unit:\"%s\"}", id, value, unit);
    }
}
