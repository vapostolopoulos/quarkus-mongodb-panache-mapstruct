package dto;

public class EmbbSliceProfileInstance extends SliceProfileInstance {

    private final double downlinkThroughput;
    private final double uplinkThroughput;

    public EmbbSliceProfileInstance(double downlinkThroughput, double uplinkThroughput) {
        super(SliceServiceType.EMBB);
        this.downlinkThroughput = downlinkThroughput;
        this.uplinkThroughput = uplinkThroughput;
    }

    public double getDownlinkThroughput() {
        return downlinkThroughput;
    }

    public double getUplinkThroughput() {
        return uplinkThroughput;
    }

    @Override
    public String toString() {
        return String.format(
                "{sliceServiceType:\"%s\", downlinkThroughput:\"%f\", uplinkThroughput:\"%f\"}", sliceServiceType, downlinkThroughput, uplinkThroughput);
    }
}
