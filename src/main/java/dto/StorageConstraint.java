package dto;

public class StorageConstraint extends CloudConstraint {

    public StorageConstraint(String id, int value, String unit) {
        super(id, value, unit);
    }
}
