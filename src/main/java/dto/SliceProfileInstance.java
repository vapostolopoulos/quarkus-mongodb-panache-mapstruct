package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;


public abstract class SliceProfileInstance {

    protected final SliceServiceType sliceServiceType;

    protected SliceProfileInstance(SliceServiceType sliceServiceType) {
        this.sliceServiceType = sliceServiceType;
    }

    @JsonIgnore
    public SliceServiceType getSliceServiceType() {
        return sliceServiceType;
    }
}
