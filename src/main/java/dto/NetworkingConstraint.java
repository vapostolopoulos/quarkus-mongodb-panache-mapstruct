package dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


public class NetworkingConstraint {

    private final String endpointId;

    // Use endpoint for serialization on runtime types on this demo as I want these models to be identical to the ones in OSS
    // In OSS we don't serialize internal models thus there is no need for this
    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            property = "endpointId",
            include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
    @JsonSubTypes(
            value = {
                    @JsonSubTypes.Type(value = EmbbSliceProfileInstance.class, name = "EMBB"),
                    @JsonSubTypes.Type(value = UrllcSliceProfileInstance.class, name = "URLLC")
            })
    @Schema(oneOf = {EmbbSliceProfileInstance.class, UrllcSliceProfileInstance.class})
    private final SliceProfileInstance sliceProfileInstance;

    @JsonCreator
    public NetworkingConstraint(String endpointId, SliceProfileInstance sliceProfileInstance) {
        this.endpointId = endpointId;
        this.sliceProfileInstance = sliceProfileInstance;
    }

    public String getEndpointId() {
        return endpointId;
    }

    public SliceProfileInstance getSliceProfileInstance() {
        return sliceProfileInstance;
    }

    @Override
    public String toString() {
        return String.format(
                "{endpointId:\"%s\", %s}", endpointId, sliceProfileInstance);
    }
}
