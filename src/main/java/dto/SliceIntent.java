package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.net.URL;
import java.util.List;
import java.util.UUID;

public class SliceIntent {

    private final URL callbackUrl;
    private final CloudConstraints cloudConstraints;
    private final List<NetworkingConstraint> networkingConstraints;
    @Schema(readOnly = true)
    @JsonProperty(access = Access.READ_ONLY)
    private UUID id;

    public SliceIntent(UUID id, URL callbackUrl, CloudConstraints cloudConstraints, List<NetworkingConstraint> networkingConstraints) {
        this.id = id;
        this.callbackUrl = callbackUrl;
        this.cloudConstraints = cloudConstraints;
        this.networkingConstraints = networkingConstraints;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public URL getCallbackUrl() {
        return callbackUrl;
    }

    public CloudConstraints getCloudConstraints() {
        return cloudConstraints;
    }

    public List<NetworkingConstraint> getNetworkingConstraints() {
        return networkingConstraints;
    }

    @Override
    public String toString() {
        return String.format("{id: \"%s\", callbackUrl: \"%s\", %s, %s}", id, callbackUrl, cloudConstraints, networkingConstraints);
    }
}
