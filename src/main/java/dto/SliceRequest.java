package dto;

public class SliceRequest {

    private final SliceIntent sliceIntent;
    private final SliceRequester sliceRequester;

    public SliceRequest(SliceIntent sliceIntent, SliceRequester sliceRequester) {
        this.sliceIntent = sliceIntent;
        this.sliceRequester = sliceRequester;
    }

    public SliceIntent getSliceIntent() {
        return sliceIntent;
    }


    public SliceRequester getSliceRequester() {
        return sliceRequester;
    }

    @Override
    public String toString() {
        return String.format("{%s, sliceRequester: \"%s\"}", sliceIntent, sliceRequester);
    }
}
