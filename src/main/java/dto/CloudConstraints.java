package dto;

import java.util.Optional;

public class CloudConstraints {

    private final CpuConstraint cpuConstraint;
    private final StorageConstraint storageConstraint;

    public CloudConstraints(
            CpuConstraint cpuConstraint,
            StorageConstraint storageConstraint
    ) {
        this.cpuConstraint = cpuConstraint;
        this.storageConstraint = storageConstraint;
    }

    public Optional<CpuConstraint> getCpuConstraint() {
        return Optional.ofNullable(cpuConstraint);
    }

    public Optional<StorageConstraint> getStorageConstraint() {
        return Optional.ofNullable(storageConstraint);
    }


    @Override
    public String toString() {
        var description = new StringBuilder();

        if (getCpuConstraint().isPresent()
                || getStorageConstraint().isPresent()) {
            description.append("[");
            getCpuConstraint()
                    .ifPresent(constraint -> description.append(String.format("%s", constraint)));
            getStorageConstraint()
                    .ifPresent(constraint -> description.append(String.format(", %s", constraint)));
            description.append("]");
        }

        return description.toString();
    }
}
