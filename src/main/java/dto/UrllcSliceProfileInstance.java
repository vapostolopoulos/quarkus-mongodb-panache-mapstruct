package dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public class UrllcSliceProfileInstance extends SliceProfileInstance {

    private final float upperDelayBoundInMs;

    @JsonCreator
    public UrllcSliceProfileInstance(float upperDelayBoundInMs) {
        super(SliceServiceType.URLLC);
        this.upperDelayBoundInMs = upperDelayBoundInMs;
    }

    public float getUpperDelayBoundInMs() {
        return upperDelayBoundInMs;
    }

    @Override
    public String toString() {
        return String.format(
                "{upperDelayBound:\"%f\"} ", upperDelayBoundInMs);
    }
}
