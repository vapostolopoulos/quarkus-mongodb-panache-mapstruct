package mapper;

import dto.CloudConstraint;
import dto.CpuConstraint;
import dto.EmbbSliceProfileInstance;
import dto.SliceProfileInstance;
import dto.SliceRequest;
import dto.UrllcSliceProfileInstance;
import entity.CloudConstraintEntity;
import entity.CpuConstraintEntity;
import entity.EmbbSliceProfileInstanceEntity;
import entity.SliceProfileInstanceEntity;
import entity.SliceRequestEntity;
import entity.StorageConstraintEntity;
import entity.UrllcSliceProfileInstanceEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.SubclassExhaustiveStrategy;
import org.mapstruct.SubclassMapping;

import java.util.Optional;

@Mapper(componentModel = "cdi", injectionStrategy = InjectionStrategy.FIELD, subclassExhaustiveStrategy = SubclassExhaustiveStrategy.RUNTIME_EXCEPTION)
public interface SliceRequestMapper {

    SliceRequestEntity toEntity(SliceRequest sliceRequest);

    @InheritInverseConfiguration
    SliceRequest toModel(SliceRequestEntity sliceRequestEntity);

    @SubclassMapping(source = EmbbSliceProfileInstance.class, target = EmbbSliceProfileInstanceEntity.class)
    @SubclassMapping(source = UrllcSliceProfileInstance.class, target = UrllcSliceProfileInstanceEntity.class)
    SliceProfileInstanceEntity toEntity(SliceProfileInstance sliceProfileInstance);

    /*
    There is possibly a bug in mapstruct because although toEntity() works as intended with "SubclassExhaustiveStrategy.RUNTIME_EXCEPTION", toModel() does not.
    I had to write the supposedly auto-generated toModel(), embbSliceProfileInstanceEntityToEmbbSliceProfileInstance() and
    urllcSliceProfileInstanceEntityToUrllcSliceProfileInstance by hand because the auto generated ones contained a compilation error.
    To see the error remove the aforementioned methods, add the following compile and inspect the SliceRequestMapperImpl under /target:
        @InheritInverseConfiguration
        default SliceProfileInstance toModel(SliceProfileInstanceEntity sliceProfileInstanceEntity);
     */
    default SliceProfileInstance toModel(SliceProfileInstanceEntity sliceProfileInstanceEntity) {

        if (sliceProfileInstanceEntity instanceof EmbbSliceProfileInstanceEntity) {
            return embbSliceProfileInstanceEntityToEmbbSliceProfileInstance((EmbbSliceProfileInstanceEntity) sliceProfileInstanceEntity);
        } else if (sliceProfileInstanceEntity instanceof UrllcSliceProfileInstanceEntity) {
            return urllcSliceProfileInstanceEntityToUrllcSliceProfileInstance((UrllcSliceProfileInstanceEntity) sliceProfileInstanceEntity);
        } else {
            throw new IllegalArgumentException("Not all subclasses are supported for this mapping. Missing for " + sliceProfileInstanceEntity.getClass());
        }
    }

    default EmbbSliceProfileInstance embbSliceProfileInstanceEntityToEmbbSliceProfileInstance(EmbbSliceProfileInstanceEntity embbSliceProfileInstanceEntity) {

        final var downlinkThroughput = embbSliceProfileInstanceEntity.getDownlinkThroughput();
        final var uplinkThroughput = embbSliceProfileInstanceEntity.getUplinkThroughput();

        return new EmbbSliceProfileInstance(downlinkThroughput, uplinkThroughput);
    }

    default UrllcSliceProfileInstance urllcSliceProfileInstanceEntityToUrllcSliceProfileInstance(
            UrllcSliceProfileInstanceEntity urllcSliceProfileInstanceEntity) {

        final var upperDelayBoundInMs = urllcSliceProfileInstanceEntity.getUpperDelayBoundInMs();

        return new UrllcSliceProfileInstance(upperDelayBoundInMs);
    }

    default <T extends CloudConstraintEntity, R extends CloudConstraint> T unwrapOptionalCloudConstraint(Optional<R> optional) {

        if (optional.isEmpty()) {
            return null;
        }

        final var cloudConstraint = optional.orElseThrow();
        final var id = cloudConstraint.getId();
        final var value = cloudConstraint.getValue();
        final var unit = cloudConstraint.getUnit();

        if (cloudConstraint instanceof CpuConstraint) {
            return (T) new CpuConstraintEntity(id, value, unit);
        } else {
            return (T) new StorageConstraintEntity(id, value, unit);
        }
    }
}
