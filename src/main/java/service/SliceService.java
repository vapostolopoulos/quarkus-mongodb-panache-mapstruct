package service;

import dto.SliceRequest;

import java.util.UUID;

public interface SliceService {

    SliceRequest add(SliceRequest sliceRequest);

    SliceRequest get(UUID sliceId);

    void delete(UUID sliceId);
}
