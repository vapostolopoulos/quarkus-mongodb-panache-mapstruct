package service;

import dto.SliceRequest;
import mapper.SliceRequestMapper;
import repository.SliceRequestStore;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@ApplicationScoped
public class SliceServiceImpl implements SliceService {

    @Inject
    SliceRequestStore store;

    @Inject
    SliceRequestMapper mapper;

    @Override
    public SliceRequest get(UUID sliceId) {
        final var sliceRequestEntity = store.get(sliceId).orElseThrow();

        final var sliceRequest = mapper.toModel(sliceRequestEntity);

        return sliceRequest;
    }

    @Override
    public SliceRequest add(SliceRequest sliceRequest) {
        final var sliceRequestEntity = mapper.toEntity(sliceRequest);

        store.persist(sliceRequestEntity);

        return sliceRequest;
    }

    @Override
    public void delete(UUID sliceId) {
        store.delete(sliceId);
    }
}
