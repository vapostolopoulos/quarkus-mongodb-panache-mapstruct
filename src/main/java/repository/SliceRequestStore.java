package repository;

import entity.SliceRequestEntity;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class SliceRequestStore implements PanacheMongoRepository<SliceRequestEntity> {

    public Optional<SliceRequestEntity> get(UUID sliceId) {
        return find("sliceIntent._id", sliceId.toString()).singleResultOptional();
    }

    public void delete(UUID sliceId) {
        delete("sliceIntent._id", sliceId.toString());
    }
}
